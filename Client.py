import socket
import json
import os

HEADER = 64
PORT = 5050
FORMAT = 'utf-8'
DISCONNECT_MESSAGE = "!DISCONNECT"
SERVER = socket.gethostbyname(socket.gethostname())
ADDR = (SERVER, PORT)

class Client:
    def __init__(self):
        self.client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.client.connect(ADDR)
        self.running = True
        self.logged_in = False
        self.username = None
        self.role = None
        self.user_id = None
        self.listen_server()

    def listen_server(self):
        response = self.client.recv(2048).decode(FORMAT)
        print(json.loads(response)["message"])

    def send(self, msg):
        self.clear_screen()
        message = msg.encode(FORMAT)
        msg_length = len(message)
        send_length = str(msg_length).encode(FORMAT)
        send_length += b' ' * (HEADER - len(send_length))
        self.client.send(send_length)
        self.client.send(message)
        response = self.client.recv(2048).decode(FORMAT)
        response_data = json.loads(response)
        print(f"Command: {msg}")
        self.pretty_print(response_data)
        if "message" in response_data and response_data["message"] == "Server stopping":
            self.running = False
        if "message" in response_data and response_data["message"] == "Login successful":
            self.logged_in = True
            self.username = response_data["username"]
            self.role = response_data["role"]
            self.user_id = response_data["user_id"]
        if "message" in response_data and response_data["message"] == "Registration successful":
            print("You can now log in with your new credentials.")

    def pretty_print(self, data):
        if "commands" in data:
            print("commands:")
            for command, description in data["commands"].items():
                print(f"    {command}: {description}")
        else:
            for key, value in data.items():
                print(f"{key}: {value}")

    def clear_screen(self):
        os.system('cls' if os.name == 'nt' else 'clear')

    def disconnect(self):
        self.send(DISCONNECT_MESSAGE)

    def login(self):
        username = input("Enter username: ")
        password1 = input("Enter password: ")
        self.send(f"login {username} {password1}")

    def register(self):
        username = input("Enter username: ")
        password1 = input("Enter password: ")
        password2 = input("Confirm password: ")
        if password1 == password2:
            self.send(f"register {username} {password1}")
        else:
            print("Passwords do not match")

    def send_message(self):
        recipient = input("Enter recipient username: ")
        message = input("Enter your message (max 250 characters): ")
        if len(message) > 250:
            print("Message is too long.")
        else:
            self.send(f"send_message {recipient} {message}")

    def show_messages(self):
        self.send("show_messages")

    def delete_message(self):
        message_id = input("Enter the ID of the message to delete: ")
        self.send(f"delete_message {message_id}")

if __name__ == "__main__":
    client = Client()
    while client.running:
        if client.logged_in:
            msg = input("Enter command (or 'help' for list of commands): ")
            if msg == "send_message":
                client.send_message()
            elif msg == "show_messages":
                client.show_messages()
            elif msg == "delete_message":
                client.delete_message()
            elif msg == "logout":
                client.logged_in = False
                client.username = None
                client.role = None
                client.user_id = None
            else:
                client.send(msg)
        else:
            msg = input("Enter command (type 'login' or 'register' to log in or register): ")
            if msg == "login":
                client.login()
            elif msg == "register":
                client.register()
            else:
                client.send(msg)
        if msg == "stop":
            client.disconnect()
            break
