import socket
import threading
import json
import time
import datetime
import uuid

HEADER = 64
PORT = 5050
SERVER = socket.gethostbyname(socket.gethostname())
ADDR = (SERVER, PORT)
FORMAT = 'utf-8'
DISCONNECT_MESSAGE = "!DISCONNECT"
VERSION = "0.1.0"
CREATION_DATE = f"{datetime.date.today()}"

class Server:
    def __init__(self):
        self.server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server.bind(ADDR)
        self.start_time = time.time()
        self.commands = {
            "uptime": self.uptime,
            "info": self.info,
            "help": self.help,
            "stop": self.stop,
            "login": self.login,
            "register": self.register,
            "send_message": self.send_message,
            "show_messages": self.show_messages,
            "delete_message": self.delete_message
        }
        self.running = True
        self.logged_in_users = {}
        self.initialize_database()

    def initialize_database(self):
        try:
            with open('database.json', 'r') as f:
                users = json.load(f)
        except FileNotFoundError:
            with open('database.json', 'w') as f:
                json.dump({}, f)

    def handle_client(self, conn, addr):
        print(f"[NEW CONNECTION] {addr} connected.")
        conn.send(json.dumps({"message": "Welcome! Type 'help' to see available commands."}).encode(FORMAT))

        connected = True
        logged_in = False
        user_id = None
        while connected:
            try:
                msg_length = conn.recv(HEADER).decode(FORMAT)
                if msg_length:
                    msg_length = int(msg_length)
                    msg = conn.recv(msg_length).decode(FORMAT)
                    if msg == DISCONNECT_MESSAGE or msg == "stop":
                        response = self.stop()
                        connected = False
                        self.running = False
                    elif msg.startswith("login"):
                        _, username, password = msg.split(" ", 2)
                        response = self.login(username, password)
                        if response.get("message") == "Login successful":
                            logged_in = True
                            user_id = response.get("user_id")
                            self.logged_in_users[conn] = user_id
                    elif msg.startswith("register"):
                        _, username, password = msg.split(" ", 2)
                        response = self.register(username, password)
                    elif msg.startswith("send_message"):
                        if logged_in:
                            _, recipient, message = msg.split(" ", 2)
                            response = self.send_message(user_id, recipient, message)
                        else:
                            response = {"error": "You must log in first."}
                    elif msg == "show_messages":
                        if logged_in:
                            response = self.show_messages(user_id)
                        else:
                            response = {"error": "You must log in first."}
                    elif msg.startswith("delete_message"):
                        if logged_in:
                            _, message_id = msg.split(" ", 1)
                            response = self.delete_message(user_id, int(message_id))
                        else:
                            response = {"error": "You must log in first."}
                    elif msg == "help":
                        response = self.help(logged_in)
                    elif logged_in:
                        if msg in self.commands:
                            response = self.commands[msg]()
                        else:
                            response = {"error": "Unknown command"}
                    else:
                        response = {"error": "You must log in first. Use 'login' or 'register'."}
                    print(f"[{addr}] {msg}")
                    conn.send(json.dumps(response).encode(FORMAT))

            except (ValueError, ConnectionResetError, ConnectionAbortedError) as e:
                print(f"[ERROR] {e}")
                break

        if conn in self.logged_in_users:
            del self.logged_in_users[conn]
        conn.close()

    def start(self):
        self.server.listen()
        print(f"[LISTENING] Server is listening on {SERVER}")
        while self.running:
            conn, addr = self.server.accept()
            thread = threading.Thread(target=self.handle_client, args=(conn, addr))
            thread.start()
            print(f"[ACTIVE CONNECTIONS] {threading.active_count() - 1}")

    def uptime(self):
        uptime_seconds = time.time() - self.start_time
        return {"uptime": round(uptime_seconds, 2)}

    def info(self):
        return {"version": VERSION, "creation_date": CREATION_DATE}

    def help(self, logged_in):
        commands = {
            "login": "logs in a user",
            "register": "registers a new user",
            "help": "returns the list of available commands"
        }
        if logged_in:
            commands.update({
                "uptime": "returns the uptime of the server",
                "info": "returns the version and creation date of the server",
                "stop": "stops the server",
                "send_message": "sends a message to another user",
                "show_messages": "shows your messages",
                "delete_message": "deletes a message by ID"
            })
        return {"commands": commands}

    def login(self, username, password):
        try:
            with open('database.json', 'r') as f:
                users = json.load(f)
            for user_id, user_data in users.items():
                if user_data["username"] == username and user_data["password"] == password:
                    role = user_data["role"]
                    return {"message": "Login successful", "role": role, "username": username, "user_id": user_id}
            return {"error": "Invalid username or password"}
        except FileNotFoundError:
            return {"error": "Database file not found"}

    def register(self, username, password):
        try:
            with open('database.json', 'r') as f:
                users = json.load(f)
            for user_data in users.values():
                if user_data["username"] == username:
                    return {"error": "Username already exists"}
            user_id = str(uuid.uuid4())
            users[user_id] = {"username": username, "password": password, "role": 0, "messages": []}
            with open('database.json', 'w') as f:
                json.dump(users, f, indent=4)
            return {"message": "Registration successful"}
        except FileNotFoundError:
            return {"error": "Database file not found"}

    def send_message(self, sender_id, recipient_username, message):
        if len(message) > 250:
            return {"error": "Message exceeds 250 characters"}
        try:
            with open('database.json', 'r') as f:
                users = json.load(f)
            recipient_id = None
            for user_id, user_data in users.items():
                if user_data["username"] == recipient_username:
                    recipient_id = user_id
                    break
            if recipient_id is None:
                return {"error": "Recipient not found"}
            recipient_messages = users[recipient_id]["messages"]
            if len(recipient_messages) >= 5:
                return {"error": "Recipient's message inbox is full"}
            message_id = len(recipient_messages) + 1
            recipient_messages.append({"id": message_id, "from": users[sender_id]["username"], "message": message})
            with open('database.json', 'w') as f:
                json.dump(users, f, indent=4)
            return {"message": "Message sent successfully"}
        except FileNotFoundError:
            return {"error": "Database file not found"}

    def show_messages(self, user_id):
        try:
            with open('database.json', 'r') as f:
                users = json.load(f)
            messages = users[user_id]["messages"]
            return {"messages": messages}
        except FileNotFoundError:
            return {"error": "Database file not found"}

    def delete_message(self, user_id, message_id):
        try:
            with open('database.json', 'r') as f:
                users = json.load(f)
            messages = users[user_id]["messages"]
            message_to_delete = next((msg for msg in messages if msg["id"] == message_id), None)
            if message_to_delete is not None:
                messages.remove(message_to_delete)
                with open('database.json', 'w') as f:
                    json.dump(users, f, indent=4)
                return {"message": "Message deleted successfully"}
            else:
                return {"error": "Message not found"}
        except FileNotFoundError:
            return {"error": "Database file not found"}

    def stop(self):
        print("Server is stopping...")
        self.running = False
        return {"message": "Server stopping"}

if __name__ == "__main__":
    print("[STARTING] server is starting...")
    server = Server()
    server.start()
    print("Server has stopped.")
